import React, { Component } from 'react'
import { dataShoe } from './data'
import List from './List'

export default class ShoeShop extends Component {
    state = {
        shoeArr: dataShoe,
        detail: null,
        cart: []
    }
    handleDetail = (shoe) => {
        const index = this.state.shoeArr.findIndex((item) => {
            return item.id == shoe.id;
        })
        if (index !== -1) {
            this.setState({ detail: this.state.shoeArr[index] })
        }
    }
    handleAddToCart = (shoe) => {
        const cloneCart = [...this.state.cart];
        const index = cloneCart.findIndex((item) => {
            return item.id == shoe.id
        });

        if (index == -1) {
            const data = { ...shoe, number: 1 }
            cloneCart.push(data)
        } else {
            cloneCart[index].number++
        }

        this.setState({
            cart: cloneCart
        })
    }
    handleTangSoLuong = (shoe) => {
        const cloneCart = [...this.state.cart];
        const index = cloneCart.findIndex((item) => {
            return item.id == shoe.id
        })

        if (index !== -1) {
            cloneCart[index].number++
        }
        this.setState({
            cart: cloneCart
        })
    }
    handleGiamSoLuong = (shoe) => {
        const cloneCart = [...this.state.cart];
        const index = cloneCart.findIndex((item) => {
            return item.id == shoe.id
        })
        if (cloneCart[index].number == 1) {
            cloneCart.splice(index, 1)
        } else {
            cloneCart[index].number--
        }
        this.setState({
            cart: cloneCart,
        })
    }
    handleDeleteCart = (shoe) => {
        const cloneCart = [...this.state.cart];
        const index = cloneCart.findIndex((item) => {
            return item.id == shoe.id
        })
        cloneCart.splice(index, 1);
        this.setState({
            cart: cloneCart,
        })
    }
    render() {
        return (
            <div class="container">
                <List data={this.state.shoeArr}
                    handleDetail={this.handleDetail}
                    detail={this.state.detail}
                    cart={this.state.cart} handleAddToCart={this.handleAddToCart} handleTangSoLuong={this.handleTangSoLuong} handleGiamSoLuong={this.handleGiamSoLuong}
                    handleDeleteCart={this.handleDeleteCart} />
            </div>
        )
    }
}
